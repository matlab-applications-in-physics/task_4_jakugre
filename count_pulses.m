function [number_of_pulses] = count_pulses(file_name,numbers,edges,bias_value)
    file = fopen(strcat(file_name,'.dat'));

    i = 0;
    %Create variables for counting number of pulses and maximal size of
    %data that can be collected to buffor
    number_of_pulses = 0;
    max_size = 1000000;
    
    %Creating loop that goes untill the end of file
    while ~feof(file)
        fseek(file,max_size*i,'bof');
        data = fread(file,max_size,'uint8');
        
        %Creating variable for storing average value from certain amount of
        %data
        avg = 0;
        
        
        %Take avery 50 elements, substract bias value from it normal values
        %oscilate over 0, then average those 50 values and if that
        %average is over 25, count it as a pulse
        for k = [1:50:length(data)-50]
            avg = sum(data(k:k+49) - bias_value)/50;
            if avg > 20
                number_of_pulses = number_of_pulses + 1;
            end
        end
    
        i = i + 1;
    end
    fclose(file);
end