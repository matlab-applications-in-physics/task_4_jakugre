function [bias_value] = bias(file_name,numbers,edges)
    file = fopen(strcat(file_name,'.dat'));
    %Find most common value in all numbers that occur in the file
    most_common_value = find(numbers == max(numbers));
    
    
    sum = 0;
    elements_sum = 0;
    
    %For the most common valaue calculate weighted average in range +- 5,
    %becasue values oscilate around most common value
    for i = most_common_value-5:most_common_value+5
        sum = sum + numbers(i)*edges(i);
        elements_sum = elements_sum + numbers(i);
    end
    
    
    bias_value = sum/elements_sum;
    
end
    









