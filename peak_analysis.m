%MATLAB 2020b
%name: peak_analysis
%author: jakugre
%date: 2020-12-6
%version: v1.2

file_name = 'Co60_1350V_x20_p7_sub';
file = fopen(strcat(file_name,'.dat'));

%Defining size of data for buffor
max_size = 1000000;

%Creating variable for keeping values from data
numbers = 0;
i = 0;

%Creating loop that goes untill the end of file
while ~feof(file)
    %Move to certain position in file
    fseek(file,max_size*i,'bof');
    %Read amount of data defined by max size
    data = fread(file,max_size,'uint8');
   
    %Get numbers and amount of them in 2 arrays
    [num,edges] = histcounts(data,'BinLimits',[0,250],'BinWidth',1);
    %Add numbers got from read data to main numbers array
    numbers = numbers + num;
    i = i + 1;
end
 
%Create histogram based on numbers and edges obtained from buffor
histogram('BinEdges',edges,'BinCounts',numbers);
pdf_name = strcat(file_name,'_histogram');

%Add labels to axis
xlabel('values');
ylabel('amount');
fclose(file);

%Save histogram to pdf
saveas(gcf,pdf_name,'pdf');


%Get bias value
bias_value = bias(file_name,numbers,edges);
    
%Count pulses
number_of_pulses = count_pulses(file_name,numbers,edges,bias_value);

%Write results to file
result_file_name = strcat(file_name,'_analysis_result.dat');
result_file = fopen(result_file_name,'w');
fprintf(result_file,'File name: %s \n',strcat(file_name,'.dat'));
fprintf(result_file,'Bias value: %.3f \n', bias_value);
fprintf(result_file,'Number of pulses: %.f \n',number_of_pulses);
fclose(result_file);

